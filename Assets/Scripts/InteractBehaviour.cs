using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractBehaviour : MonoBehaviour
{
    [SerializeField]
    private GameObject doorText;

    [SerializeField]
    private GameObject lightText;

    void Start()
    {
        doorText.SetActive(false);
        lightText.SetActive(false);
    }

    void Update()
    {
        RaycastHit hit;

        if(Physics.Raycast(transform.position, transform.forward, out hit, 1.5f))
        {
            if(hit.collider.CompareTag("Door"))
            {
                doorText.SetActive(true);
                if (Input.GetMouseButtonDown(0))
                {
                    DoOpenDoor(hit.collider.GetComponent<Animator>());
                }
            }
            else
            {
                doorText.SetActive(false);
            }

            if(hit.collider.CompareTag("Activator"))
            {
                lightText.SetActive(true);
                if (Input.GetMouseButtonDown(0))
                {
                    Lights currentLights = hit.collider.GetComponentInParent<Lights>();

                    currentLights.EnableOrDisable();
                }
            }
            else
            {
                lightText.SetActive(false);
            }
        }
        else
        {
            doorText.SetActive(false);
            lightText.SetActive(false);
        }
    }

    void DoOpenDoor(Animator anim)
    {
        anim.SetTrigger("OpenClose");
    }
}
