using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lights : MonoBehaviour
{
    [SerializeField]
    private Light[] lights;

    public void EnableOrDisable()
    {
        for (int i = 0; i < lights.Length; i++)
        {
            if (lights[i].isActiveAndEnabled)
            {
                lights[i].enabled = false;
            }
            else
            {
                lights[i].enabled = true;
            }
        }
    }
}
