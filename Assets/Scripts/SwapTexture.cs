using System.Linq;
using UnityEngine;

public class SwapTexture : MonoBehaviour
{
    [SerializeField]
    private Material[] materials;
    private int materialIndex = 0;
    [SerializeField]
    private Renderer renderer;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
                materialIndex++;
                materialIndex = materialIndex % materials.Length;
                renderer.material = materials[materialIndex];
        }
    }
}
